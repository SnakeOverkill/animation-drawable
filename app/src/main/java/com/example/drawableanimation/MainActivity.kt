package com.example.drawableanimation

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.drawableanimation.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val animationDrawable = binding.imageOn.background as AnimationDrawable

        binding.imageOn.setOnClickListener {
            animationDrawable.stop()
            binding.imageOff.visibility = View.VISIBLE
            binding.imageOn.visibility = View.GONE
        }

        binding.imageOff.setOnClickListener {
            animationDrawable.start()
            binding.imageOff.visibility = View.GONE
            binding.imageOn.visibility = View.VISIBLE
        }

    }

}